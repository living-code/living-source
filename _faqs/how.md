---
title: How?
layout: default
date: 14-04-2024
excerpt:
id:
category: FAQ
tags:
image:
author:
description:
---

## The link between size and impact

Big companies that operate on a massive massive scale are banned from using *Living Source* materials to effectively
reduce the overall level of harm to people and the planet.

Although small-scale use also poses risks, the total impact of the harm is much smaller in comparison.

This is why *Living Source* explicitly bans large-scale use rather than specific activities or purposes like some
other licenses do.

By banning large corporations *Living Source* eliminates significant risks of harms by targeting problematic practices
caused by scale alone.

## Easier Monitoring

It is much easier for copyright owners to monitor and enforce their licenses among a relatively smaller number of large
businesses than to police countless small entities looking for unethical or prohibited practices.

Large companies report the number of employees in official returns so no special analysis is need to find out if a
company is using *Living Source* code in breach of the terms.

## Worker Welfare

Large companies, by definition employ more people and so the level of exploitation is probably higher in these
companies.

By targeting large-scale operations, *Living Source* intends to achieve the most substantial decrease in harms and
motivate global change.

