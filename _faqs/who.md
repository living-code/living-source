---
title: Who?
layout: default
date: 14-04-2024
excerpt:
id:
category:
tags:
image:
author:
description:
---

**Living Code**, **Living Source** and the **Living License** came out of my wanting to share my book,
**[Living Patterns](https://livingpatternsbook.com)** freely, without using a pro-corporate *Open Source* license or
committing to the *naive dogma* of *Free Software*.

But **remember** I am just
[one veteran British web programmer and researcher](https://zotero.org/mat_k_witts){:target="_blank"},
***not a lawyer***.

What works for me, might not work for you.

+ BTW: the software for this website is courtesy of the folk at [Jekyll](https://jekyllrb.com/){:target="_blank"}.
It's a shame so much of the software out there is not **Living Source**, but you never know, maybe more will be,
one day!

