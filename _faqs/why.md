---
title: Why?
layout: default
date: 24-04-2024
excerpt:
id:
category:
tags:
image:
author:
description:
---

**Living Source** is part of **Living Code**, a movement that extends the **Open Source** and **Free Culture** movements.

Instead of promoting ***indiscriminate access***, a **Living Source** license explicitly
prohibits **large-scale business use** while allowing everyone else to do pretty much whatever they like.

**Living Source** aims to serve the public good:

1. Promoting fairer working practices
2. Improving economic efficiency
3. Protecting public interest
3. Keeping in mind security, health, safety and environmental implications.

Large businesses pose particular threats that are less obvious in smaller businesses simply due to the scale of
operations, resources, financial and legal power and influence.

## Corporate Power

Some large businesses have too much control over a sector or market. Monopolistic practices result in higher prices,
lower quality products, less innovation, and fewer choices for the public.

## Holding Back Innovation

Dominance creates barriers to entry for anyone else. Big businesses seek to control access to resources,
like technology and access to networks that are unattainable for ordinary people and smaller organizations.

## Economic Control

Large businesses have disproportionate influence over prices. This is used to *'starve others out'* and
then they raise prices when everyone else has been killed off.

The bigger the business, the more they are able to access finance and negotiate better terms than individuals,
smaller outfits and non-profits.

## Political Influence

Large corporations lobby for legislation that favors their own interests at the expense of smaller entities and the
public good.

They exert pressure on regulatory authorities to have regulations that serve their interests. This includes trading
laws, labor policies and environmental regulations.

## Free Riding

Large businesses access *'Open Source'* and *Free Software* with impunity. They control vast supply chains and 
networks, giving them the power to treat individuals, small businesses, communities and workers unfairly.

Complicated corporate structures also let them exploit tax laws. They take advantage of loopholes in legislation to 
minimize the tax they pay that could be used for wider, public benefit.

## In Summary
 
The specific threat posed by large businesses is the way they exploit the public, the economy, workers, the environment,
tax and other regulations, sometimes with disasterous consequences for large populations. They present significant
environmemtal hazards in terms of massive pollution and large-scale resource depletion.
