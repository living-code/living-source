---
title: What?
layout: default
date: 14-04-2024
excerpt:
id:
category:
tags:
image:
author:
description:
---

***Living Source*** is for work distributed on websites, servers, digital platforms, apps and so on.

There is already a simple to use, permissive, public copyright licensing scheme prohibiting large businesses called the
 ***Living License*** which allows most people to do pretty much whatever they like with *Living Source*.

The only requirement is to include either the license text in full, **or** a clickable 
link to <http://livinglicense.com>.


| **Can** | Use commercially, modify, adapt, distribute, sublicense[^sub], use privately[^private], use a patent[^pat], add a warranty[^warranty] |
| **Must** | Include license or clickable link to <http://livinglicense.com> |
| **Cannot** | Hold Workers liable |

***IMPORTANT: Please make sure you have read and understood the implications of the Living License before you fix your work to it. Hire your own legal adviser if you are unsure of the suitability of the Living License for your own needs.***

***The Living License cannot be revoked!***

If you think the *Living License* is not right for you, [other Living Source licenses](living-source-licenses) are available.

[^sub]: Modified works may be licensed with additional restrictions, (e.g. commercial, 'non-commercial' or 'ethical' clauses).
[^private]: Private use includes special 'free uses', such as [quotations](https://en.wikipedia.org/wiki/Right_to_quote) in news reports, [fair use](https://en.wikipedia.org/wiki/Fair_use) for teaching and study purposes, [first sale](https://en.wikipedia.org/wiki/First_sale), [work for hire](https://en.wikipedia.org/wiki/Work_for_hire) and some other proprietary uses such as evaluation, testing, trials and [trade secrets](https://en.wikipedia.org/wiki/Trade_secret).
[^pat]: Having a patent grant may be useful with some software because it protects Workers against legal claims.
[^warranty]: A warranty may be added, for example if additional products or  services are bundled with the work.
