---
title: Living Source?
layout: home
---

***Living Source*** prohibits *large-scale business use* so we can continue to work together without adding to the
economies of scale and concentrations of corporate wealth typified by *'Big Tech'*.

