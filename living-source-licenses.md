---
title: Other Living Source licenses
layout: home
---

***Living Source*** is about avoiding large-scale harms of *'Big Business'*.

The licenses on this page may or may not *explicitly* ban large-scale business use, but they all have features that may
indirecly have a similar afffect of constraining other harmful practices.

This page is not an endorsement of these licenses for any particular use. The usual legal disclaimers definitely still
apply.

Please make sure you understand the implications of copyright licensing for your own situation and seek legal advice
as necessary BEFORE you fix your work to any license, whether it's a normal commercial license, *'Open Source'*,
*'Free Software'*, *'Ethical Source'* or anything else!!

Please **[raise an issue on GitLab](https://gitlab.com/living-code/living-source/-/issues){:target="_blank"}** if you
would like to comment on this list, request an update to an existing license, or submit a new license for evaluation.

## Licenses already evaluated

| License details                                                                                                           | Identifier                   |
|---------------------------------------------------------------------------------------------------------------------------|------------------------------|
| [Anti-Capitalist Software License 1.4](https://anticapitalist.software)                                                   | ACSL-1.4                     |
| [CC BY-NC](https://creativecommons.org/licenses/by-nc/4.0/)                                                               | CC-BY-NC-4.0                 |
| [CC BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/)                                                         | CC-BY-NC-ND-4.0              |
| [ACAB License](https://github.com/jgrey4296/acab/blob/main/LICENSE)                                                       | ACABL                        |
| [CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/)                                                         | CC-BY-NC-SA-4.0              |
| [CoopCycle License](https://wiki.coopcycle.org/en:license)                                                                | CCL                          |
| [Cooperative Nonviolent Public License](https://git.pixie.town/thufie/npl-builder)                                        | CNPL                         |
| [Cooperative Nonviolent Public License No Attributions](https://git.pixie.town/thufie/npl-builder)                        | CNPL-NA                      |
| [Anti-Capitalist Attribution Cooperative License](https://noroadhome.itch.io/acaclicense)                                 | ACACL                        |
| [Cooperative Software License](http://xwx.moe/c/csl-2019-05-07.txt)                                                       | CSL                          |
| [IANG License](http://iang.info/en/license.html)                                                                          | IANGL                        |
| [Non-Commercial Government Licence](http://www.nationalarchives.gov.uk/doc/non-commercial-government-licence/version/2/)  | NCGL-UK-2.0                  |
| [Peer Production Licence](https://civicwise.org/peer-production-licence)                                                  | PPL                          |
| [PolyForm Noncommercial License 1.0.0](https://polyformproject.org/licenses/noncommercial/1.0.0)                          | PolyForm-Noncommercial-1.0.0 |

## Licenses waiting initial evaluation

| License details                                                                                                                                                                                                      | Identifier  |
|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------|
| [Indie Code Catalog Free License](https://indiecc.com/free/2.0.0)                                                                                                                                                    | ICCFL-2.0   |
| [Prosperity Public License](https://prosperitylicense.com/)                                                                                                                                                          | PPL-3.0.0   |
| [The Keypunch Public License](https://kpl.dgold.eu/)                                                                                                                                                                 | KPL-1.0.1   |
| [Opinionated Queer License v1.0 Software License v1.0](https://oql.avris.it/license/v1.0)                                                                                                                            | OQL-1.0     |
| [Copyfair Software License v1.0](https://github.com/CopyFairCorp/copyfair/blob/master/CFSLv0.template.md)                                                                                                            | CFSL-1.0    |
| [Copysol License 2.0](http://wiki.p2pfoundation.net/Copysol_License#TERMS_AND_CONDITIONS)                                                                                                                            | CL-2.0      |
| [Common Good Public License](http://cgpl.org/)                                                                                                                                                                       | CGPL        |
| [Commons Clause (License Condition) 1.0](https://commonsclause.com/)                                                                                                                                                 | N/A         |
| [Fair Source License, version 0.9](https://fair.io)                                                                                                                                                                  | FSL-0.9     |
| [LA FABRIQUE DES MOBILITÉS RECIPROCAL LICENCE](https://wiki.p2pfoundation.net/Fab_Mob_Reciprocal_License_for_the_Legal_Contractualisation_of_Commons#APPENDIX_II:_LA_FABRIQUE_DES_MOBILIT.C3.89S_RECIPROCAL_LICENCE) | FMRL        |
| [People's Ethical Appropriation Reciprocity License](https://github.com/sharingspace/PEARL/blob/master/License.txt)                                                                                                  | PEARL-0.2   |

## Other licences for possible evaluation

| License details                                                                                               | 
|---------------------------------------------------------------------------------------------------------------|
| 966.ICU                                   <https://github.com/996icu/996.ICU>                                 |
| Do No Harm License                        <https://github.com/raisely/NoHarm>                                 |
| Hippocratic License                       <https://firstdonoharm.dev/>                                        |
| inno3                                     <https://framagit.org/inno3/tm-contract-for-oss-maintainers>        |
| ml5                                       <https://michaelweinberg.org/blog/2021/01/12/ml5-call-for-comment/> |
| Atmosphere Software License Version 0.4   <https://www.open-austin.org/atmosphere-license/>                   |

